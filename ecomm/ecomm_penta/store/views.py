from itertools import product
from django.shortcuts import render,redirect,HttpResponseRedirect
from django.contrib.auth.hashers import make_password

from . models.product import Coupon, Product,Coupon
from . models.category import Category
from . models.customer import Customer
from . models.orders import Order,Cart,CartItems

from django.views import  View
from django.contrib.auth.hashers import  check_password
from django.http import JsonResponse
# from . middlewares.auth import auth_middleware


class Addcart(View):
    def post(self , request):
        message="Item added to your cart"
        if not request.session.get('customer'):
            return JsonResponse({"status":False})
        product_id = request.POST.get('id')
        sts = request.POST.get('sts')
        pdt_price=Product.objects.get(id=product_id).price
        user_id=request.session.get('customer')
        try:
            cart=Cart.objects.get(user_id=user_id,ordered=False)
            cart_id=cart.id
        except:
            cartnew=Cart.objects.create(user_id=user_id)
            cart_id=cartnew.id

        obj_cart=Cart.objects.get(id=cart_id)

        try:
            item=CartItems.objects.get(user_id=user_id,product_id=product_id,cart__ordered=False)
            if sts =="1":
                item.price +=pdt_price 
                item.quantity+=1
                print(pdt_price ,"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
                obj_cart.total_price+=pdt_price 
                
            else:
                item.price -=pdt_price 
                item.quantity-=1
                message="Item removed from your cart"
                obj_cart.total_price-=pdt_price 
            obj_cart.save()
            item.save()
            if item.quantity <= 0:
                CartItems.objects.get(id=item.id).delete()

        except:
            CartItems(cart_id=cart_id,user_id=user_id,product_id=product_id,price=pdt_price).save()
        
        return JsonResponse({"status":True,"message":message})
        
    

class Delete_cart(View):
    def post(self,request,pid):
        objdel=CartItems.objects.get(id=pid)
        delprice=objdel.price
        objcart=Cart.objects.get(id=objdel.cart.id)
        objcart.total_price -=delprice
        objcart.save()
        objdel.delete()
        return redirect('cart')

class single_product(View):
    def get(self,request):
        prdtId=request.GET.get('pdtId')
        print(request.GET,".................................oijoi")
        data={}
        d=Product.objects.get(id=prdtId)
        data['product']=d
        return render(request,'single_item.html',data)

class Index(View):
    def get(self , request):
        pdts=Product.get_all_products()
        category=Category.get_all_categories()
        data={}
        data['products']=pdts
        data['categories']=category
        return render(request,'index.html',data)


def signup(request):
    if request . method =='POST':
        postData = request.POST
        first_name = postData.get('firstname')
        last_name = postData.get('lastname')
        phone = postData.get('phone')
        email = postData.get('email')
        password = postData.get('password')
        customer = Customer(first_name=first_name,
                            last_name=last_name,
                            phone=phone,
                            email=email,
                            password=password)
        # customer.register()
        # validation
        value = {
            'first_name': first_name,
            'last_name': last_name,
            'phone': phone,
            'email': email
        }
        # error_message = None
        error_message = validateCustomer(customer)
        if not error_message:
            print(first_name, last_name, phone, email, password)
            customer.password = make_password(customer.password)
            customer.register()
            request.session['customer'] = customer.id
            return redirect('viewindex')
        else:
            data = {
                'error': error_message,
                'values': value
            }
            return render(request, 'signup.html', data)
    return render(request,'signup.html')


def validateCustomer(customer):
        error_message = None
        if (not customer.first_name):
            error_message = "First Name Required !!"
        elif len(customer.first_name) < 4:
            error_message = 'First Name must be 4 char long or more'
        elif not customer.last_name:
            error_message = 'Last Name Required'
        elif len(customer.last_name) < 4:
            error_message = 'Last Name must be 4 char long or more'
        elif not customer.phone:
            error_message = 'Phone Number required'
        elif len(customer.phone) < 10:
            error_message = 'Phone Number must be 10 char Long'
        elif len(customer.password) < 6:
            error_message = 'Password must be 6 char long'
        elif len(customer.email) < 5:
            error_message = 'Email must be 5 char long'
        elif customer.isExists():
            error_message = 'Email Address Already Registered..'
        # saving

        return error_message


class Login(View):
    return_url = None
    def get(self , request):
        Login.return_url = request.GET.get('return_url')
        return render(request , 'login.html')

    def post(self , request):
        email = request.POST.get('email')
        password = request.POST.get('password')
        customer = Customer.get_customer_by_email(email)
        error_message = None
        if customer:
            flag = check_password(password, customer.password)
            if flag:
                request.session['customer'] = customer.id

                if Login.return_url:
                    return HttpResponseRedirect(Login.return_url)
                else:
                    Login.return_url = None
                    return redirect('viewindex')
            else:
                error_message = 'Email or Password invalid !!'
        else:
            error_message = 'Email or Password invalid !!'

        print(email, password)
        return render(request, 'login.html', {'error': error_message})

def logout(request):
    request.session.clear()
    return redirect('login')

# class Cart(View):
#     def get(self , request):
#         ids = list(request.session.get('cart').keys())
#         products = Product.get_products_by_id(ids)
#         print(products)
#         return render(request , 'cart.html' , {'products' : products} )

class Cartview(View):
    def get(self , request):
        user_id=request.session.get('customer')
        cart_products=CartItems.objects.filter(user_id=user_id,cart__ordered=False)

        return render(request , 'cart.html' , {'products' : cart_products} )

class CheckoutView(View):
    def get(self , request ):
        user_id=request.session.get('customer')
        cart_products=CartItems.objects.filter(user_id=user_id,cart__ordered=False)
        all_cupons=Coupon.objects.all()
        return render(request , 'checkout.html',{'products' : cart_products,"all_cupons":all_cupons}  )


class OrderView(View):
    def get(self , request ):
        user_id=request.session.get('customer')
        cart_products=CartItems.objects.filter(user_id=user_id,cart__ordered=False)
        return render(request , 'order.html',{'products' : cart_products}  )

