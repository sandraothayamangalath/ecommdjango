from django.db import models
from .category import Category
from.varients import *


class Coupon(models.Model):
    title = models.CharField(max_length=200)
    code = models.CharField(max_length=20)
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    discount = models.FloatField()
    max_discount = models.FloatField(blank=True,null=True)
    min_amount_to_spent = models.FloatField()
    # max_uses = models.IntegerField(blank=True)
    # max_uses_user = models.IntegerField(blank=True)
    is_percentage = models.BooleanField(default=True)
    # is_for_specific_users = models.BooleanField(default=False)
    # is_for_specific_products = models.BooleanField(default=False)
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField()

    @staticmethod
    def get_all_coupons():
        return Coupon.objects.filter(is_active=True)


class Tax(models.Model):
    tax_name=models.CharField(max_length=50)
    percentage=models.IntegerField()


class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField(default=0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
    description = models.CharField(max_length=100, default='' , null=True , blank=True)
    image = models.ImageField(upload_to='uploads/products/')
    stock=models.IntegerField(default=100)
    quantity=models.CharField(max_length=100,null=True,blank=True)
    quantity_type=models.ForeignKey(QuantityVariant,null=True,blank=True,on_delete=models.PROTECT)
    colour_type=models.ForeignKey(ColourVarient,null=True,blank=True,on_delete=models.PROTECT)
    size_type=models.ForeignKey(SizeVarient,null=True,blank=True,on_delete=models.PROTECT)
    Brand_name=models.ForeignKey(BrandVarient,null=True,blank=True,on_delete=models.PROTECT)
    # Coupon_id=models.ForeignKey(Coupon,null=True,blank=True,on_delete=models.PROTECT)
    is_deleted=models.BooleanField(default=False)

    @staticmethod
    def get_products_by_id(ids):
        return Product.objects.filter(id__in =ids)

    @staticmethod
    def get_all_products():
        return Product.objects.filter(is_deleted=False)

    @staticmethod
    def get_all_products_by_categoryid(category_id):
        if category_id:
            return Product.objects.filter(category = category_id)
        else:
            return Product.get_all_products()

