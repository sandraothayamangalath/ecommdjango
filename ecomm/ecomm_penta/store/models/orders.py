from django.db import models
from .product import Product
from .customer import Customer
import datetime
from django.dispatch import receiver
from django.db.models.signals import pre_save,post_delete





class Cart(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    total_price = models.FloatField(default=0)

    def __str__(self):
        return str(self.user.first_name) + " " + str(self.total_price)


class CartItems(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE) 
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    price = models.FloatField(default=0)
    quantity = models.IntegerField(default=1)
    
    def __str__(self):
        return str(self.user.first_name) + " " + str(self.product.name)


# @receiver(pre_save, sender=CartItems)
# def correct_price(sender, **kwargs):
#     cart_items = kwargs['instance']
#     price_of_product = Product.objects.get(id=cart_items.product.id)
#     # cart_items.price = cart_items.quantity * float(price_of_product.price)
#     total_cart_items = CartItems.objects.filter(user = cart_items.user,cart__ordered=False )
#     cart = Cart.objects.get(id = cart_items.cart.id)
#     cart.total_price += price_of_product.price
#     cart.save()


class Order(models.Model):
    product = models.ForeignKey(Product,
                                on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer,
                                 on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    price = models.IntegerField()
    address = models.CharField(max_length=50, default='', blank=True)
    phone = models.CharField(max_length=50, default='', blank=True)
    date = models.DateField(default=datetime.datetime.today)
    status = models.BooleanField(default=False)

    # def placeOrder(self):
    #     self.save()

    # @staticmethod
    # def get_orders_by_customer(customer_id):
    #     return Order.objects.filter(customer=customer_id).order_by('-date')

