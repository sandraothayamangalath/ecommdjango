# Generated by Django 3.2.7 on 2022-11-15 12:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_coupon'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='Coupon_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='store.coupon'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='max_discount',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
