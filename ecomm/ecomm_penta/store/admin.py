from django.contrib import admin
from .models.product import Product,Coupon,Tax
from .models.category import Category
from .models.customer import Customer
from .models.orders import Order,CartItems,Cart
# from .models.coupon import Coupon


class AdminProduct(admin.ModelAdmin):
    list_display = ['name', 'price', 'category']



class AdminCategory(admin.ModelAdmin):
    list_display = ['name']


# Register your models here.
admin.site.register(Product, AdminProduct)
admin.site.register(Category , AdminCategory)
admin.site.register(Customer )
admin.site.register(Order )
admin.site.register(Coupon )
admin.site.register(Tax)
admin.site.register(CartItems)
admin.site.register(Cart)


