
from django.urls import path,include
from . import views
# from .views.login import Login , logout
from .middlewares.auth import  auth_middleware


urlpatterns = [
    path('', views.Index.as_view(),name='viewindex'),
    path('signup',views.signup),
    path('login', views.Login.as_view(), name='login'),
    path('logout', views.logout , name='logout'),
    path('add_cart', views.Addcart.as_view() , name='addcart'),
    path('checkout', views.CheckoutView.as_view() , name='checkout'),

    
    path('cart', auth_middleware(views.Cartview.as_view()) , name='cart'),
    path('single_product', views.single_product.as_view(),name='singleProduct'),
    path('delete/<pid>', views.Delete_cart.as_view(),name='delete'),

    
]